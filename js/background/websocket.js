export const WSClient = {
	_socket: null,
	connected : false,
	_lastReceive: Date.now(),
	HeartbeatTimeOut: 10,
	_reconnect: null,
	ws_supported:true,
	address:"wss://ws.steam-trader.com",
	handlers: {},
	alt_connect:null,
	onConnectCallback: null,
	
	Connect: function(callback)
	{
		try
		{
			if(WSClient._socket !== null && (WSClient._socket.readyState === 0 || WSClient._socket.readyState === 1))
			{
				return;
			}

			if(WSClient.onConnectCallback == null)
			{
				WSClient.onConnectCallback = callback;
			}
			
			WSClient._socket = new WebSocket(WSClient.address, "text");
			WSClient._socket.onopen = function() { WSClient._OnOpen(); WSClient.onConnectCallback(); };
			WSClient._socket.onmessage = function(message){ WSClient._OnMessage(message) };
			WSClient._socket.onclose = function(e) { WSClient._OnClose() };
			WSClient._socket.onerror = function(error){ WSClient._OnError(error) };
		}
		catch(e){console.log(e);}
	},
	
	Send: function(message)
	{
		if(WSClient.ws_supported && message instanceof WSMsg && WSClient._socket.readyState === 1)
		{
		    let data;

			try
			{
				data = message.Serialize();
				//console.info("send:",data);
				WSClient._socket.send(data);
			}
			catch(e)
			{
			    console.error({ 'message': e.message, 'data': data, 'file': 'websocket.js' })
			}
		}
	},
	
	AddHandler: function(type, func, jsonAnswer = true)
	{
		WSClient.handlers[type] = {'func' : func, 'json' : jsonAnswer };
	},

	DeleteAllHandlers: function()
	{
		WSClient.handlers = {};
	},
	
	_OnOpen: function()
	{
		//console.log('old on ws open')
		WSClient.connected = true;
		clearTimeout(WSClient._reconnect);
	},
	
	_OnMessage: function(e)
	{
		WSClient._lastReceive = Date.now();
		let type, data;

		try
		{
			type = parseInt(e.data.substr(0, 2), 16);
			data = e.data.substr(2);
			var message = new WSMsg(type, data);
			//console.info("receive:",e.data);
			var handler = WSClient.handlers[message.type];
			if(handler !== undefined)
			{
				handler.func(handler.json === true ? JSON.parse(message.data) : message.data);
			}
		}
		catch(e)
		{
		    console.error({
				'message':e.message,
				'data':data,
				'type' : type,
				'file':'websocket.js'
			});
		}
	},
	
	_OnClose: function()
	{
		WSClient.connected = false;
		//console.log('wsclient on close');
		clearTimeout(WSClient._reconnect);
		WSClient._reconnect = setTimeout(function(){
			if(WSClient._socket !== null)
				WSClient._socket.close();
			
			WSClient.Connect();
		}, WSClient.HeartbeatTimeOut * 1000);
	},
	
	_OnError: function(error)
	{
		if(error.message !== undefined)
			console.error({'message':error.message,'file':'websocket.js'});
	},
};

export function WSMsg(itype, idata)
{
	var type = null;
	var data = null;
	
	this.type = itype;
	this.data = idata;
	
	this.Serialize = function()
	{
		var type = (parseInt(this.type) + 0x100).toString(16).substr(-2).toUpperCase();
		var data = type + (typeof(this.data) === "object" ? JSON.stringify(this.data) : this.data);
		return data;
	}
};

export const WSMsgType = {
	Auth : 1,
    SendP2pOffer : 27
};